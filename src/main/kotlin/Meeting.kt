open class Meeting(val meetingName: String, val location: Location = Location("")) {
    /*constructor(
            meetingName: String,
            location: Location
    ) : this(meetingName)*/

    fun addParticipant(participant: Participant) {
        if (verifyParticipant(participant)) {
            println("Added: ${participant.name}")
        }
    }

    private fun verifyParticipant(participant: Participant): Boolean {
        println("try to verify")
        return true
    }
}

class Location(val address: String)

class PersonalReview(meetingName: String, val employee: Participant, reviewers: List<Participant>, location: Location = Location("")) : Meeting(meetingName, location)

