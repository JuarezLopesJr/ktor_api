class Name(val name: String) {
    /*   var name: String = ""
           set(value: String) {
               if (value.isBlank()) throw IllegalArgumentException()
               field = value
           }*/
    // this init will do the validation on every instantiation of the class Name
    init {
        if (name.isBlank()) throw IllegalArgumentException()
    }
}