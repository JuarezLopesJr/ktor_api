class Participant(val name: Name, val email: String) {

    val canonicalEmail: String
        get() = email.toUpperCase()
}